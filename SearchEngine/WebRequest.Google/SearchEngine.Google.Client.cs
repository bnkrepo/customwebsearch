﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BNK.CWS.Common.Containers;

namespace SearchEngine.Webrequest.Google
{
    internal class GoogleSearchEngineClient
    {
        internal List<SearchResultEntry> ExecuteQuery(string strQuery, string strFileFormat)
        {
            List<SearchResultEntry> searchResults = new List<SearchResultEntry>();

            var client = new GoogleSearchClient(strQuery);
			  
            foreach (var result in client.Query())
            {
                searchResults.Add(new SearchResultEntry(result.Text, result.CleanUri.AbsoluteUri, ""));
            }

            return searchResults;
        }
    }
}
