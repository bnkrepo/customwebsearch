﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;
using BNK.CWS.Common.Containers;

namespace SearchEngine
{
    internal class BingCustomSearchEngine
    {
        internal List<SearchResultEntry> ExecuteQuery(string strQuery, string strFileFormat)
        {
            List<SearchResultEntry> searchResults = new List<SearchResultEntry>();

            var bing = new BingSearchContainer(new Uri("https://api.datamarket.azure.com/Bing/Search/")) { Credentials = new NetworkCredential(SearchSettings.BingKey, SearchSettings.BingKey) };

            var query = bing.Web(strQuery, null, null, null, null, null, null, string.IsNullOrEmpty(strFileFormat) ? null : strFileFormat);
            var results = query.Execute();
            
            foreach (var result in results)
            {
                searchResults.Add(new SearchResultEntry(result.Title, result.Url, result.Description));
            }

            return searchResults;
        }       
    }

}
