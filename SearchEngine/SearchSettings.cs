﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SearchEngine
{
    internal class SearchSettings
    {
        public static string TorrentSite
        {
            get { return (string)search.Default["TorrentSite"]; }
        }

        public static string VideoSite
        {
            get { return (string)search.Default["VideoSite"]; }
        }

        public static string GoogleId
        {
            get { return (string)search.Default["GoogleId"]; }
        }

        public static string GoogleKey
        {
            get { return (string)search.Default["GoogleKey"]; }
        }

        public static string SafeWords
        {
            get { return (string)search.Default["SafeWords"]; }
        }

        public static string SafeHosts
        {
            get { return (string)search.Default["SafeHosts"]; }
        }

        public static string YouTubeKey
        {
            get { return (string)search.Default["YouTubeKey"]; }
        }

        public static string BingKey
        {
            get { return (string)search.Default["BingKey"]; }
        }
    }
}
