﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BNK.CWS.Common.Containers;
using SearchEngine.Webrequest.Google;

namespace SearchEngine
{
    public class ContentSearchEngine
    {
        public List<SearchResultEntry> ExecuteQuery(string strQuery, string fileFormat)
        {
            List<SearchResultEntry> result = new List<SearchResultEntry>();

            // Google API
            GoogleCustomSearchEngine google = new GoogleCustomSearchEngine();
            List<SearchResultEntry> result1 = google.ExecuteQuery(strQuery, fileFormat);

            // Bing API
            BingCustomSearchEngine bing = new BingCustomSearchEngine();
            List<SearchResultEntry> result2 = bing.ExecuteQuery(strQuery, fileFormat);

            //Google Client
            GoogleSearchEngineClient googleClient = new GoogleSearchEngineClient();
            List<SearchResultEntry> result3 = googleClient.ExecuteQuery(strQuery, fileFormat);

            if (result1 != null && result1.Count > 0)
                result.AddRange(result1);

            if (result2 != null && result2.Count > 0)
                result.AddRange(result2);

            if (result3 != null && result3.Count > 0)
                result.AddRange(result3);

            return result;

        }
    }
}
