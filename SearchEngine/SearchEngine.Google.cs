﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BNK.CWS.Common.Containers;
using Google.Apis.Customsearch.v1;
using Google.Apis.Customsearch.v1.Data;

namespace SearchEngine
{
    internal class GoogleCustomSearchEngine
    {

        private List<SearchResultEntry> GetWebLinks(SearchInput searchInput)
        {
            try
            {
                CustomsearchService customSearchService = new CustomsearchService(new Google.Apis.Services.BaseClientService.Initializer() { ApiKey = SearchSettings.GoogleKey });
                string strQuery = string.IsNullOrEmpty(searchInput.SiteToSearchIn) ?
                                                        searchInput.GetShortSearchPhrase() :
                                                        searchInput.GetSiteSearchPhrase();

                Debug.WriteLine(">> " + strQuery + " <<");

                CseResource.ListRequest listRequest = customSearchService.Cse.List(strQuery);

                listRequest.Cx = SearchSettings.GoogleId;
                Search searchResult = listRequest.Execute();
                List<SearchResultEntry> resultList = new List<SearchResultEntry>();

                if (searchResult.Items == null)
                    return null;

                //*
                foreach (var item in searchResult.Items)
                {
                    SearchResultEntry result = new SearchResultEntry
                                                (
                                                    item.Title,
                                                    item.Link,
                                                    "", 
                                                    IsVideoLink(item.Pagemap),
                                                    GetVideoDuration(item.Pagemap)
                                                );

                    resultList.Add(result);
                }
                //*/
                return resultList;// searchResult.Items.ToList().Select(item => item.Link).ToList();
            }
            catch (Exception exp)
            {
                return null;
            }
        }

        private bool IsVideoLink(IDictionary<string, IList<IDictionary<string, object>>> data)
        {
            if (data.ContainsKey("videoobject") == false)
                return false;
            else
                return true;
        }

        private int GetVideoDuration(IDictionary<string, IList<IDictionary<string, object>>> data)
        {
            if (data.ContainsKey("videoobject") == false)
                return -1;

            var value = data["videoobject"][0];

            if (value.ContainsKey("duration") == false)
                return -1;

            string time = (string)value["duration"];

            if (time.StartsWith("PT") == true)
                time = time.Remove(0, 2);

            int hIndex = time.IndexOf('H');
            string H = "0";

            if (hIndex != -1)
            {
                H = time.Substring(0, hIndex);
                time = time.Remove(0, hIndex);
            }

            int mIndex = time.IndexOf('M');
            string M = "";

            if (mIndex != -1)
            {
                M = time.Substring(0, mIndex);
                time = time.Remove(0, mIndex+1);
            }

            int sIndex = time.IndexOf('S');
            string S = "";

            if (sIndex != -1)
            {
                S = time.Substring(0, sIndex);
            }

            int hours, minutes, seconds;

            Int32.TryParse(H, out hours);
            Int32.TryParse(M, out minutes);
            Int32.TryParse(S, out seconds);

            return hours * 3600 + minutes * 60 + seconds; // convert to seconds
        }

        private List<SearchResultEntry> GetTorrentLinks(SearchInput input)
        {
            List<string> links = new List<string>();          
            char[] seperator = new char[1] { ',' };
            string[] tokens = SearchSettings.TorrentSite.Trim().Split(seperator, StringSplitOptions.RemoveEmptyEntries);

            return GetSpecificSiteLinks(input, tokens);
        }

        private List<SearchResultEntry> GetSpecificVideoSiteLinks(SearchInput input)
        {            
            char[] seperator = new char[1] { ',' };
            string[] tokens = SearchSettings.VideoSite.Trim().Split(seperator, StringSplitOptions.RemoveEmptyEntries);

            return GetSpecificSiteLinks(input, tokens);
        }

        private List<SearchResultEntry> GetSpecificSiteLinks(SearchInput input, string[] sites)
        {
            List<SearchResultEntry> links = new List<SearchResultEntry>();

            foreach (string site in sites)
            {
                input.SiteToSearchIn = site;
                var list = GetWebLinks(input);

                if (list != null)
                    links.AddRange(list);
            }

            return links;
        }

        //
        internal List<SearchResultEntry> ExecuteQuery(string strQuery, string strFileFormat)
        {
            List<SearchResultEntry> links = new List<SearchResultEntry>();

            SearchInput input = new SearchInput { Title = strQuery, Format = strFileFormat};

            // Online web search
            var webLinks = GetWebLinks(input);

            if (webLinks != null)
                links.AddRange(webLinks);

            // Torrents search
            var torrentsLinks = GetTorrentLinks(input);

            if (torrentsLinks != null)
                links.AddRange(torrentsLinks);

            // Site Specific Search
            var siteLinks = GetSpecificVideoSiteLinks(input);

            if (siteLinks != null)
                links.AddRange(siteLinks);

            // remove safe sites            
            char[] seperator = new char[1] { ',' };
            List<string> safeTokens = SearchSettings.SafeWords.Trim().Split(seperator, StringSplitOptions.RemoveEmptyEntries).ToList();

            List<string> safeTokens1 = SearchSettings.SafeHosts.Trim().Split(seperator, StringSplitOptions.RemoveEmptyEntries).ToList();
            safeTokens.AddRange(safeTokens1);

            bool bFound = false;

            var filteredLinks = new List<SearchResultEntry>();

            foreach (var link in links)
            {
                bFound = false;
                foreach (var safeWord in safeTokens)
                {
                    if (link.Link.Contains(safeWord) == true)
                    {
                        Debug.WriteLine(">> " + link + " -> " + safeWord);

                        bFound = true;
                        break;
                    }
                }

                if (bFound == false)
                    filteredLinks.Add(link);
            }

            return filteredLinks;
        }
    }
}
