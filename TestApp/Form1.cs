﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BNK.CWS.Common.Containers;
using SearchEngine;
using SearchEngine.Webrequest.Google;

namespace TestApp
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ContentSearchEngine search = new ContentSearchEngine();
            var result = search.ExecuteQuery(textBox1.Text, "");

            foreach (var line in result)
            {
                textBox2.Text += line.Link + " [" + line.Duration.ToString() + "]";
                textBox2.Text += Environment.NewLine;
            }

           
        }
    }
}
