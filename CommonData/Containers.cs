﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BNK.CWS.Common.Containers
{
    public enum SearchContentType
    {
        Video,
        Audio,
        EBook,
        Game,
        Other
    }

    public class SearchInput
    {
        public string Title { get; set; }           // movie name/book title/music album title etc.
        public string Language { get; set; }
        public int ReleaseYear { get; set; }     // Year released
        public string Format { get; set; }          // .mp4, .mkv, .iso, mp3,.pdf etc.
        public string SiteToSearchIn { get; set; }
        public SearchContentType Type { get; set; }

        public string GetShortSearchPhrase()
        {
            return Title + " + " + Language;
        }

        public string GetLongSearchPhrase()
        {
            return Title + " + " + Language + " + " + ReleaseYear + " + " + Format;
        }

        public string GetSiteSearchPhrase()
        {
            if (string.IsNullOrEmpty(SiteToSearchIn) == true)
                return GetShortSearchPhrase();
            else
                return "site:" + SiteToSearchIn + " + " + Title + " + " + Language;

        }
    }

    public class SearchResultEntry
    {
        public string Title { get; set; }
        public string Link { get; set; }
        public string Description { get; set; }
        public bool IsVideoLink { get; set; }
        public int Duration { get; set; }

        public SearchResultEntry(string title, string link, string description, bool isVideoLink = false, int duration = -1)
        {
            Title = title;
            Link = link;
            Description = description;
            IsVideoLink = isVideoLink;
            Duration = duration;
        }
    }
}
